package api

import (
	"MyWorkSpace/Golang-Demo/app/handler"
	"MyWorkSpace/Golang-Demo/app/models"
	"fmt"
	"github.com/labstack/echo"
	"net/http"
	"strconv"
)

func FindAll(ctx echo.Context) error {
	result, err := handler.GetAll()
	if err != nil {
		fmt.Println(err)
		return err
	}
	return ctx.JSON(http.StatusCreated, result)
}

func GetBookById(ctx echo.Context) error {
	var id int64
	id, _ = strconv.ParseInt(ctx.Param("id"), 10, 64)
	result, err := handler.GetById(id)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return ctx.JSON(http.StatusCreated, result)
}

func CreateBook(ctx echo.Context) error {
	book := new(models.Book)
	if err := ctx.Bind(book); err != nil {
		return err
	}

	err := handler.Insert(book)
	if err != nil {
		fmt.Sprintln(err)
		return err
	}
	return ctx.String(http.StatusOK, "ok")
}

func UpdateBook(ctx echo.Context) error {
	book := new(models.Book)
	if err := ctx.Bind(book); err != nil {
		return err
	}
	id,_ := strconv.ParseInt(ctx.Param("id"), 10, 64)

	err := handler.Update(book, id)
	if err != nil {
		fmt.Sprintln(err)
		return err
	}
	return ctx.String(http.StatusOK, "ok")
}

func DeleteBook(ctx echo.Context) error {
	id,_ := strconv.ParseInt(ctx.Param("id"), 10, 64)
	err := handler.Delete(id)
	if err != nil {
		fmt.Sprintln(err)
		return err
	}
	return ctx.String(http.StatusOK, "ok")
}