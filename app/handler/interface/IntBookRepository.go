package _interface

import "MyWorkSpace/Golang-Demo/app/models"

type BookRepository interface {
	GetAll() ([]models.Book, error)
	GetById(id int64) (models.Book, error)
	Insert(book *models.Book) error
	Update(book *models.Book, id int64) error
	Delete(id int64) error
}
