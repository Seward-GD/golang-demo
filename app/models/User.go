package models

import "time"

type (
	User struct {
		Id int64 `json:"id"`
		Email string `json:"email"`
		UserName string `json:"username"`
		Password string `json:"password"`
		CreatedAt time.Time `json:"created_at"`
		UpdatedAt time.Time `json:"updated_at"`
	}
)

func NewUser() *User {
	return &User{
		Id: 0,
		Email: "",
		UserName: "",
		Password: "",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
}
