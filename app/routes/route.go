package routes

import (
	"MyWorkSpace/Golang-Demo/app/api"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"net/http"
	"time"
)

func Route()  {
	e := echo.New()

	//Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	//CORS
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.POST, echo.DELETE},
		//AllowHeaders:
	}))

	//Root route => routes
	e.Static("/", "/static")
	e.GET("/", func(c echo.Context) error {
		return c.File("static/views/index.html")
	})
	bg := e.Group("/books")
	bg.GET("", api.FindAll)
	bg.GET("/:id", api.GetBookById)
	bg.POST("/create", api.CreateBook)
	bg.PUT("/update/:id", api.UpdateBook)
	bg.DELETE("/delete/:id", api.DeleteBook)

	//login := e.Group("/user")
	e.GET("/user/login", api.Login)

	e.GET("/attachment", func(c echo.Context) error {
		return c.Attachment("attachment.txt", "attachment.txt")
	})

	//Run Server
	s := &http.Server{
		Addr:         "127.0.0.1:80",
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}

	e.Logger.Fatal(e.StartServer(s))
}
