package handler

import (
	"MyWorkSpace/Golang-Demo/app/conf"
	"MyWorkSpace/Golang-Demo/app/models"
	"database/sql"
	"log"
)

var db *sql.DB

func Signup(user *models.User) error {
	db = conf.Config()
	defer db.Close()

	sqlStatement := `INSERT INTO users (email, username, password, updated_at, created_at) VALUES ($1, $2, $3, $4, $5)`
	_, err := db.Exec(sqlStatement, user.Email, user.UserName, user.Password, user.UpdatedAt, user.CreatedAt)

	if err != nil {
		return err
	}
	return nil
}

func Login(email string, password string) (models.User, error) {
	db := conf.Config()
	defer db.Close()

	sqlStatement := `SELECT email, username, password FROM users WHERE email=$1 AND password=$2`
	user := models.User{}
	row := db.QueryRow(sqlStatement, email,password)
	err := row.Scan(&user.Email, &user.UserName, &user.Password)

	if err == sql.ErrNoRows {
		log.Println("No rows were returned!")
	}
	if err != nil {
		return models.User{}, err
	}
	return user, nil
}
